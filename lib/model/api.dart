class BaseUrl {
  // static String url = "http://192.168.43.38/apps_helpdesk/";
  static String url = "http://192.168.43.38/apps_helpdesk/";
  static String url2 = "http://192.168.43.38/api_project/";
  static String urlLogin = url + "api/login.php";
  static String urlLogin2 = url2 + "api/login.php";
  static String urlDataTeknisi = url + "api/data_teknisi.php";
  static String urlTambahTeknisi = url + "api/tambah_teknisi.php";
  static String urlHapusTeknisi = url + "api/hapus_teknisi.php";
  static String urlEditTeknisi = url + "api/edit_teknisi.php";
  static String urlGender = url + "api/data_gender.php";
  static String urlDataAdmin = url + "api/data_admin.php";
  static String urlTambahAdmin = url + "api/tambah_admin.php";
  static String urlHapusAdmin = url + "api/hapus_admin.php";
  static String urlEditAdmin = url + "api/edit_admin.php";
  static String urlDataPC = url + "api/data_pc.php";
  static String urlTambahPC = url + "api/tambah_pc.php";
  static String urlHapusPC = url + "api/hapus_pc.php";
  static String urlEditPC = url + "api/edit_pc.php";
  static String urlDataJenis = url + "api/data_jenis.php";
  static String urlTambahJenis = url + "api/tambah_jenis.php";
  static String urlHapusJenis = url + "api/hapus_jenis.php";
  static String urlEditJenis = url + "api/edit_jenis.php";
  static String urlDataTiket = url + "api/data_tiket.php";
  static String urlTambahTiket = url + "api/tambah_tiket.php";
  static String urlEditTiket = url + "api/edit_tiket.php";
  static String urlDataSts = url + "api/data_status.php";
}
